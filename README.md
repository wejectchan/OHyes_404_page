在线预览：

-   [demo_clouds（绿色云页面）](http://wejectchan.gitee.io/ohyes_404_page/demo_clouds/404_page.html)
-   [demo_astronaut_purple(紫色宇航员)](http://wejectchan.gitee.io/ohyes_404_page/demo_astronaut_purple/404_page.html)
-   [demo_primitive(原始人)](http://wejectchan.gitee.io/ohyes_404_page/demo_primitive/index.html)

其他组件：

-   [separator_page（左右可移动分隔栏）](http://wejectchan.gitee.io/ohyes_404_page/demo_separator/separator_page.html)
-   [demo_wave（波浪效果）](http://wejectchan.gitee.io/ohyes_404_page/demo_wave/)
-   [demo_HomePageImg(滚动背景主页）](http://wejectchan.gitee.io/ohyes_404_page/demo_HomePageImg/)
-   [demo_login(登录页面）](http://wejectchan.gitee.io/ohyes_404_page/demo_login/index.html)
